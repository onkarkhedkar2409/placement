import UserRouter from '../user/user.routes';
import { Route } from './routes.types';

export const routes: Route[] = [
    new Route('/user', UserRouter),

];
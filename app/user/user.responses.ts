export enum EUserResponse {
    LOGIN_SUCCESS,
    LOGIN_FAILED
}

export const UserResponse = {
    [EUserResponse.LOGIN_FAILED]: {
        message: "EMAIL OR PASSWORD IS INCORRECT",
        statusCode: 403
    },
    [EUserResponse.LOGIN_SUCCESS]: {
        message: "LOGGED IN SUCCESFULLY"
    }
}
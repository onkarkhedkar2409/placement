import { NextFunction, Request, Response, Router } from "express";
import { CreateUserValidator,LoginValidator } from "./user.validate";
import { ICredentials, IUser } from './user.types';
import userService from "./user.service";
import { ResponseHandler } from './../utility/response-handler';

const router = Router();

// CREATE THE USER
router.post("/", CreateUserValidator, async (
    req: Request, 
    res: Response, 
    next: NextFunction
) => {
    try {
        const user = req.body as IUser;
        const result = await userService.create(user);
        res.send(new ResponseHandler(result));
    } catch (e) {
        next(e);
    }
});

// LOGGING IN THE USER
router.post("/login", LoginValidator, async (
    req: Request, 
    res: Response, 
    next: NextFunction
) => {
    try {
        const credentials = req.body as ICredentials;
        const result = await userService.login(credentials);
        res.send(new ResponseHandler(result));
    } catch (e) {
        next(e);
    }
});

export default router;
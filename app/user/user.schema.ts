import { v4 } from 'uuid';
import { IUser } from "./user.types";
 

class UserSchema{
    private users :IUser[]= [];


    create(user:IUser)
    {
        user.id= v4();
        this.users.push(user);
        return user;
    }

    getone(email:string)
    {
        const user= this.users.find(u=>u.email===email);
        return user;
    }
}

const UserModel = new UserSchema();
export default UserModel;
import userRepo from "./user.repo";
import { hash, genSalt, compare } from 'bcrypt';
import { IUser,ICredentials } from "./user.types";
import { EUserResponse, UserResponse } from "./user.responses";

const create = async (user: IUser) => {
    // change the password to in encrypted format
    const salt = await genSalt(10);
    const hashedPassword = await hash(user.password, salt);

    user.password = hashedPassword;

    const createdUser = userRepo.create(user);

    return createdUser;
}

const login = async(credentials:ICredentials)=>{
    const user = userRepo.getOne(credentials.email);

    if (!user) {
        // throw an error
        throw UserResponse[EUserResponse.LOGIN_FAILED];
    }

    // 2. compare the passwords
    const didMatch = await compare(credentials.password, user.password);

    if (!didMatch) {
        // throw an error
        throw UserResponse[EUserResponse.LOGIN_FAILED];
    }


    // 3. send out the response
    return UserResponse[EUserResponse.LOGIN_SUCCESS] ;   

}

export default {
    create,
    login
}

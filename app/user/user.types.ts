export interface IUser{
    id?:string;
    name:string;
    email:string;
    password:string;

}

export interface ICredentials {
    email: string;
    password: string;
}
import UserModel from "./user.schema";
import { IUser } from './user.types';

const create = (user: IUser) => UserModel.create(user);

const getOne = (email: string) => UserModel.getone(email);

export default {
    create,
    getOne
}